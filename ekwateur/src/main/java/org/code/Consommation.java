package org.code;

public class Consommation {

    private String typeElecritite;
    private double nbKWhGaz;

    public Consommation(String typeElecritite, double nbKWhGaz) {
        this.typeElecritite = typeElecritite;
        this.nbKWhGaz = nbKWhGaz;
    }

    public String getTypeElecritite() {
        return typeElecritite;
    }

    public double getNbKWhGaz() {
        return nbKWhGaz;
    }
}