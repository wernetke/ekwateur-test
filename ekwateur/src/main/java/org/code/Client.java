package org.code;

import java.util.ArrayList;
import java.util.List;

public abstract class Client {

    private String referenceClient;
    private List<Consommation> consommations;

    public Client(String referenceClient) {
        if (referenceClient == null || !referenceClient.matches("EKW\\d{8}")) {
            throw new IllegalArgumentException("La référence du client est invalide");
        }
        this.referenceClient = referenceClient;
        this.consommations = new ArrayList<>();
    }

    public String getReferenceClient() {
        return referenceClient;
    }

    public void ajoutConsommation(Consommation consommation) {
        consommations.add(consommation);
    }

    public abstract double getPrixKWhElectricite();

    public abstract double getPrixKWhGaz();

    public double calculerMontantFacture() {
        double montantTotal = 0;
        for (Consommation consommation : consommations) {
            double prixKWh;
            if (consommation.getTypeElecritite().equals("Electricité")) {
                prixKWh = getPrixKWhElectricite();
            } else {
                prixKWh = getPrixKWhGaz();
            }
            montantTotal += consommation.getNbKWhGaz() * prixKWh;
        }
        return montantTotal;
    }
}
