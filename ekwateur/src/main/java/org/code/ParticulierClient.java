package org.code;

public class ParticulierClient extends Client {

    private String civilite;
    private String nom;
    private String prenom;

    public ParticulierClient(String referenceClient, String civilite, String nom, String prenom) {
        super(referenceClient);
        this.civilite = civilite;
        this.nom = nom;
        this.prenom = prenom;
    }

    @Override
    public double getPrixKWhElectricite() {
        return 0.121;
    }

    @Override
    public double getPrixKWhGaz() {
        return 0.115;
    }

}