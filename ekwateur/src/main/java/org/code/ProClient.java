package org.code;

public class ProClient extends Client {
    private String siret;
    private String raisonSociale;
    private double ca;

    public ProClient(String referenceClient, String siret, String raisonSociale, double ca) {
        super(referenceClient);
        this.siret = siret;
        this.raisonSociale = raisonSociale;
        this.ca = ca;
    }

    @Override
    public double getPrixKWhElectricite() {
        if (ca > 1000000) {
            return 0.114;
        } else {
            return 0.118;
        }
    }

    @Override
    public double getPrixKWhGaz() {
        if (ca > 1000000) {
            return 0.111;
        } else {
            return 0.113;
        }
    }

}