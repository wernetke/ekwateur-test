package org.code;

public class Main {
    public static void main(String[] args) {
        ProClient proClient = new ProClient("EKW12345678", "123456789012345", "AAAA", 1500000);
        ParticulierClient particulierClient = new ParticulierClient("EKW87654321", "M.", "DU", "Kev");

        // ajout de consommations pour chaque client
        proClient.ajoutConsommation(new Consommation("Electricité", 500));
        proClient.ajoutConsommation(new Consommation("Gaz", 300));

        particulierClient.ajoutConsommation(new Consommation("Electricité", 400));
        particulierClient.ajoutConsommation(new Consommation("Gaz", 200));

        // calcul du montant total à facturer pour chaque client
        double montantPro = proClient.calculerMontantFacture();
        double montantParticulier = particulierClient.calculerMontantFacture();

        // affichage des résultats
        System.out.println("Montant à facturer pour le client pro : " + montantPro + " euros");
        System.out.println("Montant à facturer pour le client particulier : " + montantParticulier + " euros");
    }
}